
################ USART ################

# buffer size for usart_printf function
C_DEFINES += USART_PRINTF_BUFFER_SIZE=512

################ TWI ################

# transfer data size limit
# C_DEFINES += TWI_TRANSFER_MAX_LENGTH=32

################ RTC ################
# C_DEFINES += F_RTC_CLK=32768UL