#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <util/twi.h>

// ru_avrlib
#include "ru_avrlib/atmega/spi.h"
#include "ru_avrlib/atmega/usart.h"

// ru_deviceslib
#include "ru_deviceslib/lcd/st7735s.h"

#define I2C_ADDRESS       0x1c
#define I2C_BUFFER_LENGTH 32

volatile uint8_t i2c_frame[I2C_BUFFER_LENGTH];
static volatile uint8_t i2c_frame_len = 0;

static void gpio_init() {
    DDRB |= (1 << DD0);  // PB0 - st7735s rst
    DDRB |= (1 << DD1);  // PB1 - st7735s ao

    DDRB |= (1 << DD2);   // PB2 - CS
    DDRB |= (1 << DD3);   // PB3 - MOSI
    DDRB &= ~(1 << DD4);  // PB4 - MISO
    DDRB |= (1 << DD5);   // PB5 - SCK

    DDRD |= (1 << DD5);  // PD5 - display LED

    // always select st7735s
    PORTB &= ~(1 << PB2);

    // display full brightness
    PORTD |= (1 << DD5);
}

static uint8_t rst_pin_set(uint8_t value) {
    if (value)
        PORTB |= (1 << PB0);
    else
        PORTB &= ~(1 << PB0);

    return 0;
}

static uint8_t ao_pin_set(uint8_t value) {
    if (value)
        PORTB |= (1 << PB1);
    else
        PORTB &= ~(1 << PB1);

    return 0;
}

uint8_t delay_ms(uint32_t ms) {
    while (--ms) {
        _delay_ms(1);
    }
    return 0;
}

void i2c_slave_init(uint8_t address) {
    TWAR = address << 1;
    TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWINT) | (1 << TWEN);

    sei();
}

void i2c_reply(uint8_t ack) {
    if (ack) {
        TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWINT) | (1 << TWEA);
    } else {
        TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWINT);
    }
}

void i2c_release_bus(void) {
    TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWINT) | (1 << TWEA);
}

void i2c_stop() {
    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}

struct st7735s_driver st7735s_driver = {
    .rst_pin_set_func = rst_pin_set,
    .ao_pin_set_func = ao_pin_set,
    .spi_transfer_func = spi_transfer,
    .delay_ms_func = delay_ms,
};

int main(void) {
    uint8_t rc;

    struct st7735s_device display_dev = {.driver = &st7735s_driver};

    wdt_enable(WDTO_8S);

    gpio_init();

    // usart_init(9600);
    // usart_puts("st7735s-i2c-proxy\n\r");

    i2c_slave_init(I2C_ADDRESS);

    spi_init(spi_clock_div_2, 0, 0, 0);

    rc = st7735s_init(&display_dev);
    // usart_printf("st7735s_init: %s\n\r", st7735s_strerr(rc));

    st7735s_fill_display(&display_dev, 0xFFFF);
    // usart_printf("st7735s_fill_display\n\r");

    delay_ms(100);

    while (1) {
        wdt_reset();

        if (i2c_frame_len == 0) continue;

        ao_pin_set(0);
        spi_transfer(i2c_frame[0]);

        ao_pin_set(1);

        for (uint8_t i = 1; i < i2c_frame_len; ++i) {
            spi_transfer(i2c_frame[i]);
        }

        // usart_puts("i2c frame received: [");
        // for (uint8_t i = 0; i < i2c_frame_len; i++) {
        //     usart_printf(" 0x%02X ", i2c_frame[i]);
        // }
        // usart_puts("]\n\r");

        i2c_frame_len = 0;
    }

    return 0;
}

static void on_i2c_receive(uint8_t *buffer, uint8_t size) {
    if (i2c_frame_len != 0) return;

    for (uint8_t i = 0; i < size; ++i) {
        i2c_frame[i] = buffer[i];
    }

    i2c_frame_len = size;
}

ISR(TWI_vect) {
    static uint8_t buffer[I2C_BUFFER_LENGTH];
    static uint8_t index = 0;

    switch (TW_STATUS) {
        case TW_SR_SLA_ACK:             // addressed, returned ack
        case TW_SR_GCALL_ACK:           // addressed generally, returned ack
        case TW_SR_ARB_LOST_SLA_ACK:    // lost arbitration, returned ack
        case TW_SR_ARB_LOST_GCALL_ACK:  // lost arbitration, returned ack
            // enter slave receiver mode
            // indicate that rx buffer can be overwritten and ack
            index = 0;
            i2c_reply(1);
            break;
        case TW_SR_DATA_ACK:        // data received, returned ack
        case TW_SR_GCALL_DATA_ACK:  // data received generally, returned ack
            // if there is still room in the rx buffer
            if (index < I2C_BUFFER_LENGTH) {
                // put byte in buffer and ack
                buffer[index++] = TWDR;
                i2c_reply(1);
            } else {
                // otherwise nack
                i2c_reply(0);
            }
            break;
        case TW_SR_STOP:  // stop or repeated start condition received
            i2c_release_bus();

            // callback to user defined callback
            on_i2c_receive(buffer, index);
            // since we submit rx buffer to "wire" library, we can reset it
            index = 0;
            break;
        case TW_SR_DATA_NACK:        // data received, returned nack
        case TW_SR_GCALL_DATA_NACK:  // data received generally, returned nack
            // nack back at master
            i2c_reply(0);
            break;

        // Slave Transmitter
        case TW_ST_SLA_ACK:           // addressed, returned ack
        case TW_ST_ARB_LOST_SLA_ACK:  // arbitration lost, returned ack

        case TW_ST_DATA_ACK:  // byte sent, ack returned

        case TW_ST_DATA_NACK:  // received nack, we are done
        case TW_ST_LAST_DATA:  // received ack, but we are done already!
            // ack future responses
            i2c_reply(1);
            // leave slave receiver state
            break;

        case TW_NO_INFO:  // no state information
            break;
        case TW_BUS_ERROR:  // bus error, illegal stop/start
            i2c_stop();
            break;

        default: break;
    }
}