

################ lcd ################
#### hd44780 configuration ####
# Sets num of rows and collums
# C_DEFINES += HD44780_ROWS_NUM=2
# C_DEFINES += HD44780_COLUMNS_NUM=16

#### st7735s configuration ####
# Sets display dimensions
C_DEFINES += ST7735S_HEIGHT=128
C_DEFINES += ST7735S_WIDTH=128

################ rfid ################


################ oled ################
# oled display size
# C_DEFINES = OLED_DISPLAY_WIDTH=128
# C_DEFINES = OLED_DISPLAY_HEIGHT=64


################ max7219 ################
#### led8x8 configuration #### 

# Sets num of phisycal displays
# C_DEFINES += LED8x8_DISP_NUM=4

# Sets num of buffered displays
# C_DEFINES += LED8x8_EXTRA_DISP_NUM=3

# Prevents loading font from libs/ru_deviceslib/max7219/led8x8_font.h
# C_DEFINES += LED8x8_FONT_LOAD
